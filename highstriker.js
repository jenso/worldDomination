class Striker{
  constructor(target){
    this.target = target;
    this.sliderValue = 50.0;
    this.mouseColor = {"light": "fd558f", "primary": "c51162"}
    this.bearColor = {"light": "7a7cff", "primary": "304ffe"}
    this.state = "init";
    this.finishFunction = undefined;
    this.canvas = new CanvasBoard($("#graph")[0], this);
    this.color = {prim: "", light: "", altLight: ""};
    this.cor = new COR();
    this.playername = "";
  }

  onFinished(func){
    striker.finishFunction = func;
  }

  calculateMaxNewton(){
    var max = 0;
    this.recordings.forEach(function(value, idx){
      max = Math.max(Math.abs(value[0]), Math.abs(value[1]), Math.abs(value[2]));
    });
    return 200.0 * max;
  }

  finishRecording(){
    if(striker.finishFunction != undefined){
      var max = striker.calculateMaxNewton();
      striker.finishFunction(max, max*striker.sliderValue, striker.recordings);
    }
  }

  listener(e){
    striker.recordings.push([e.acceleration.x, e.acceleration.y, e.acceleration.z, striker.recordings[striker.recordings.length - 1][3] + e.interval]);
    console.debug(e);
    if(striker.recordings[striker.recordings.length - 1][3] - striker.recordings[0][3] > 10000){
      console.debug("stopped recording");
      window.removeEventListener("devicemotion", striker.listener);
      striker.finishRecording();
    }
  } 

  record(debug){
    this.clearRecordings();
    console.debug("start recording");
    window.addEventListener("devicemotion", striker.listener);
    if(debug){
      striker.recordings.push([10.0,10.0,10.0,100]);
      console.debug("skip recording; debugmode");
      window.setTimeout(striker.finishRecording, 5000);
    }

  }

  clearRecordings(){
    this.recordings = [[0.0,0.0,0.0,0.0]];
  }

  changeColor(){
    var bearPrim = {
      "r": parseInt("0x"+this.bearColor.primary.slice(0,2)),
      "g": parseInt("0x"+this.bearColor.primary.slice(2,4)),
      "b": parseInt("0x"+this.bearColor.primary.slice(4,6))
    };
    var mousePrim = {
      "r": parseInt("0x"+this.mouseColor.primary.slice(0,2)),
      "g": parseInt("0x"+this.mouseColor.primary.slice(2,4)),
      "b": parseInt("0x"+this.mouseColor.primary.slice(4,6))
    };
    var bearLight = {
      "r": parseInt("0x"+this.bearColor.light.slice(0,2)),
      "g": parseInt("0x"+this.bearColor.light.slice(2,4)),
      "b": parseInt("0x"+this.bearColor.light.slice(4,6))
    };
    var mouseLight = {
      "r": parseInt("0x"+this.mouseColor.light.slice(0,2)),
      "g": parseInt("0x"+this.mouseColor.light.slice(2,4)),
      "b": parseInt("0x"+this.mouseColor.light.slice(4,6))
    };

    striker.color.prim = {
      "r": (this.sliderValue * mousePrim.r + (1.0-this.sliderValue) * bearPrim.r),
      "g": (this.sliderValue * mousePrim.g + (1.0-this.sliderValue) * bearPrim.g),
      "b": (this.sliderValue * mousePrim.b + (1.0-this.sliderValue) * bearPrim.b)
    }
    striker.color.light = {
      "r": (this.sliderValue * mouseLight.r + (1.0-this.sliderValue) * bearLight.r),
      "g": (this.sliderValue * mouseLight.g + (1.0-this.sliderValue) * bearLight.g),
      "b": (this.sliderValue * mouseLight.b + (1.0-this.sliderValue) * bearLight.b)
    }
    striker.color.altLight = {
      "r": (this.sliderValue * bearLight.r + (1.0-this.sliderValue) * mouseLight.r),
      "g": (this.sliderValue * bearLight.g + (1.0-this.sliderValue) * mouseLight.g),
      "b": (this.sliderValue * bearLight.b + (1.0-this.sliderValue) * mouseLight.b)
    }

    //$(".bgRect").css({fill: "rgb(" + prim.r + ","+ prim.g + "," + prim.b +")"});
    $("header").css({"background-color": "rgb(" + striker.color.prim.r + ","+ striker.color.prim.g + "," + striker.color.prim.b +")"}); 
    //$(".fillup").css({fill: "rgb(" + light.r + ","+ light.g + "," + light.b +")"});
  }
}

window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();

class CanvasBoardAnimation{
  constructor(){
    this.bgColor = "#000";
    this.fgColor = "#000";
    this.textColor = "#fff";
    this.board = null;
    this.context = null;
    this.width = null;
    this.height = null;
    this.targetValue = null;
    this.currentValue = null;
    this.finishFunction = null;
    this.values = [0,20,40,60,80]
  }

  withBoard(board){
    this.board = board;
    return this;
  }
  withContext(context){
    this.context = context;
    return this;
  }
  withWidth(width){
    this.width = width;
    return this;
  }
  withHeight(height){
    this.height = height;
    return this;
  }
  withTargetValue(targetValue){
    this.targetValue = targetValue;
    return this;
  }
  withCurrentValue(currentValue){
    this.currentValue = currentValue;
    return this;
  }
  withFinishFunction(finishFunction){
    this.finishFunction = finishFunction;
    return this;
  }
  withForeroundColor(fgColor){
    this.fgColor = fgColor;
    return this;
  }
  withBackgroundColor(bgColor){
    this.bgColor = bgColor;
    return this;
  }

  animationFinished(){
    return this.currentValue < this.targetValue - 40;
  }

  nextAnimationIteration(){
    this.currentValue += 2;
    if(this.currentValue%20 == 10){
      this.values[Math.floor(this.currentValue/20.0)%this.values.length] += 100;
    }

    var self = this;
    requestAnimFrame(function() {
      self.animate();
    });
  }

  animate(){
    this.drawBackground();
    this.drawForeground();

    if(this.animationFinished()){
      this.nextAnimationIteration();
    } else {
      this.finishFunction();
    }
  }

  drawBackground(){
    this.context.fillStyle = this.bgColor;
    this.context.fillRect(0, 0, this.width, this.height);

    this.context.fillStyle = this.fgColor;
    this.context.lineWidth = 2;
    this.context.fillRect(this.width*0.3, 0, this.width*0.4, this.height);
    this.context.strokeRect(this.width*0.3, 1, this.width*0.4, this.height-2);

    this.context.beginPath();
    this.context.fillStyle = this.textColor;
    this.context.moveTo(this.width*0.25, this.height*0.45 - 2);
    this.context.lineTo(this.width*0.25, this.height*0.55 - 2);
    this.context.lineTo(this.width*0.30, this.height*0.50 - 2);
    this.context.lineTo(this.width*0.25, this.height*0.45 - 2);
    this.context.fill();
    this.context.stroke();
  }

  drawForeground(){
    var bias = (this.currentValue%100)/100.0;

    this.context.fillStyle = this.textColor;
    this.context.font = "30px Arial";
    this.context.textAlign = "center"
    for(var i = 0; i < 5; ++i){
      this.context.fillText(this.values[i], this.width*0.5, (this.height*(0.93 - (i*0.2)+bias))%this.height );
    }

    this.context.beginPath();
    this.context.strokeStyle = this.textColor;
    for(var i = -10; i < 10; ++i){
      this.context.moveTo(0.3*this.width, ((0.1*i)+bias)*this.height);
      this.context.lineTo(0.4*this.width, ((0.1*i)+bias)*this.height);
      this.context.moveTo(0.6*this.width, ((0.1*i)+bias)*this.height);
      this.context.lineTo(0.7*this.width, ((0.1*i)+bias)*this.height);
    }
    this.context.stroke();

  }

}

class CanvasBoard{
  constructor(target, striker){
    this.target = target;
    this.striker = striker;
    this.nsteps = 0;
    this.passes = -1;
    this.animation = undefined;
  }

  getPrimaryColor(){
    return "rgb(" + this.striker.color.prim.r + "," + this.striker.color.prim.g + ", " + this.striker.color.prim.b+")";
  }
  getLightColor(){
    return "rgb(" + this.striker.color.light.r + "," + this.striker.color.light.g + ", " + this.striker.color.light.b+")"; 
  }

  animate(board, ctx, width, height, maxval){
    var bgColor = this.getPrimaryColor();
    var fgColor = this.getLightColor();
    //var textColor = "rgb(" + board.striker.color.light.r + "," + board.striker.color.light.g + ", " + board.striker.color.light.b+")";
    var textColor = "#fff";
    this.animation = new CanvasBoardAnimation();
    this.animation.withBoard(board)
      .withForeroundColor(fgColor)
      .withBackgroundColor(bgColor)
      .withContext(ctx)
      .withWidth(width)
      .withHeight(height)
      .withTargetValue(maxval)
      .withCurrentValue(0)
      .withFinishFunction(function(){
        var headerText = "";
        if(maxval < 100){
          headerText = "nope, try again";
        }else if(maxval < 500){
          headerText = "better than expected";
        }else if(maxval < 1000){
          headerText = "not bad!";
        }else if(maxval < 2000){
          headerText = "impressive!";
        }else if(maxval < 4000){
          headerText = "awesome!";
        }else if(maxval < 8000){
          headerText = "WOW!";
        }else{
          headerText = "T̝͒͌̕I̭͇̮̼͛̅ͥ̇̎Ľ̗̥͖̽̌̑́͑̑́͜͝T̴̥̗̲̟̿͒ͭ̿̊̈́̈́͟";
        }
        $("h1").text(headerText);
        $("h1").removeClass("fadeout");
        $("h1").addClass("fadein");
        striker.cor.check({"key": "animationFinished", "points": Math.round(maxval)});
      });
    this.animation.animate();


    board.nsteps += 2;

    if(board.nsteps < maxval - 40){
      requestAnimFrame(function() {
        board.animation.withCurrentValue(board.nsteps+=2).animate();
        //board.animate(board, ctx, width, height, maxval);
      });
    }else{

    }
  }

  setupAndAnimate(value){
    var width = $(this.target).width();
    var height = $(this.target).height();
    var ctx = this.target.getContext("2d");
    this.target.style.width = width;
    this.target.style.height = height;
    this.target.width = width;
    this.target.height = height;
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    this.animate(this, ctx, width, height, value);

  }
}

class CorElement{
  constructor(key){
    this.key = key;
    this.next = null;
  }
  check(obj){
    if(this.key == obj.key && this.additionalChecks(obj)){
      this.handle(obj);
    }else{
      if(this.next != null){
        this.next.check(obj);
      }
    }
  }
  handle(obj){}
  additionalChecks(obj){return true;}
}

class COR extends CorElement{
  constructor(){
    super("");
    this.lastHandler = this;
    this.add(new SetupHandler())
      .add(new StartButtonHandler())
      .add(new DoneButtonHandler())
      .add(new ReadyMessageHandler())
      .add(new SetMessageHandler())
      .add(new SwingFinishedHandler())
      .add(new SwingMessageHandler())
      .add(new AnimationFinishedHandler())
      .add(new RestartButtonHandler());
  }
  add(handler){
    this.lastHandler.next = handler;
    this.lastHandler = handler;
    return this;
  }
}

class RestartButtonHandler extends CorElement{
  constructor(){
    super("restart");
  }

  handle(obj){
    $(".graphRow").addClass("fadeout");
    $("#leaderboardDiv").removeClass("fadein");
    $("#leaderboardDiv").addClass("fadeout");

    striker.state = "setup";
    striker.playername = $("#nameTextField").val();
    navigator.vibrate(100);
    $("#strengthSlider").val(50);
    $("#startButtonGroup").removeClass("fadein");
    $("h1").removeClass("fadein");
    $("#startButtonGroup").addClass("fadeout");
    $("h1").addClass("fadeout");
  }
}

class StartButtonHandler extends CorElement{
  constructor(){
    super("start");
  }

  handle(obj){
    striker.state = "setup";
    striker.playername = $("#nameTextField").val();
    navigator.vibrate(100);
    $("#strengthSlider").val(50);
    $("#startButtonGroup").removeClass("fadein");
    $("h1").removeClass("fadein");
    $("#startButtonGroup").addClass("fadeout");
    $("h1").addClass("fadeout");
  }
}

class SetupHandler extends CorElement{
  constructor(){
    super("setup");
  }
  handle(obj){
    $("h1").text("adjust as you wish");
    $("h1").addClass("fadein");
    $(".strengthAdjusterRow").removeClass("hidden");
    $(".strengthAdjusterRow").addClass("longfadein");
    $("#doneButton").removeClass("hidden");
    $("#doneButton").addClass("fadein");
    striker.sliderValue = 0.5;
    striker.changeColor(0.5);
  }
}

class DoneButtonHandler extends CorElement{
  constructor(){
    super("done");
  }

  handle(obj){
    striker.state = "readyMessage";
    $("#doneButton, h1, .strengthAdjusterRow").removeClass("fadein longfadein");
    $("h1, .strengthAdjusterRow").addClass("fadeout");   
  }
}


class ReadyMessageHandler extends CorElement{
  constructor(){
    super("readyMessage");
  }

  handle(obj){
    $(".strengthAdjusterRow").addClass("hidden");
    striker.state = "setMessage";
    $("h1").text("ready");
    $("h1").removeClass("fadeout"); 
    $("h1").addClass("fastpop");
  }
}

class SetMessageHandler extends CorElement{
  constructor(){
    super("setMessage");
  }

  handle(obj){
    striker.state = "swingMessage"
    $("h1").text("set");
  }
}
class SwingMessageHandler extends CorElement{
  constructor(){
    super("swingMessage");
  }

  handle(obj){
    navigator.vibrate(200);
    striker.state = "swing";
    $("h1").text("swing");
    $("h1").removeClass("fastpop");
    $("h1").addClass("fadeout");
    $(".progressRow").removeClass("hidden");
    striker.onFinished(function(max, modmax, vals){
      striker.cor.check({"key": "swingFinished", "modmax": modmax, "max": max});
    });
    striker.record();
  }
}

class SwingFinishedHandler extends CorElement{
  constructor(){
    super("swingFinished");
  }   

  handle(obj){
    striker.state = "finished";
    $(".progressRow").addClass("hidden");
    $(".graphRow").removeClass("fadeout");
    $(".graphRow").removeClass("hidden");
    navigator.vibrate(500);
    animateToPoints(obj.modmax);
  }
}

class AnimationFinishedHandler extends CorElement{
  constructor(){
    super("animationFinished");
  }   

  handle(obj){
    this.showLeaderboard(obj);
  }

  showLeaderboard(obj){
    $("#leaderboardDiv").removeClass("hidden");
    $("#leaderboardDiv").addClass("fadein");
    self = this;
    $.getJSON("db.php", {"add": "", "name": striker.playername, "points": obj.points}, function(data){self.populateBoard(data);});
  }

  populateBoard(data){
    this.clearBoard();
    self = this;
    data.forEach(function(entry){
      self.addEntry(entry[0], entry[1], entry[2]);
    });
  }

  clearBoard(){
    $("#leaderboard>li").remove();
  }

  addEntry(name, points, date){
    if(name == striker.playername){
      $("#leaderboard").append($('<li class="mdl-list__item mdl-list__item--two-line"> <span class="mdl-list__item-primary-content"> <span><b>'+name+'</b></span> <span class="mdl-list__item-sub-title">'+points+' Punkte</span></span></li>'));
    }else{
      $("#leaderboard").append($('<li class="mdl-list__item mdl-list__item--two-line"> <span class="mdl-list__item-primary-content"> <span>'+name+'</span> <span class="mdl-list__item-sub-title">'+points+' Punkte</span></span></li>'));
    }
  }
}

function animateToPoints(points){
  striker.canvas.setupAndAnimate(points);
}

$().ready(function(){
  striker = new Striker($("#content"));

  $("h1").on("animationiteration", function(e){
    striker.cor.check( {"key": striker.state, "animationName": e.originalEvent.animationName, "on": "animationiteration" });
  });
  $("h1").on("animationend", function(e){
    striker.cor.check( {"key": striker.state, "animationName": e.originalEvent.animationName, "on": "animationend" });
  });
  $("#startButton").on("click", function(){
    striker.cor.check( {"key": "start"});
  });
  $("#doneButton").on("click", function(){
    striker.cor.check( {"key": "done"});
  });
  $("#restartButton").on("click", function(){
    striker.cor.check( {"key": "restart"});
  });

  $("#strengthSlider").on("input", function(evt){
    striker.sliderValue = this.value/100.0;
    striker.changeColor(this.sliderValue);
//    $("#doneButton").removeClass("hidden");
//    $("#doneButton").addClass("fadein");
  });
})
