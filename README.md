# World Domination
In diesem Repository befindet sich der Quellcode, der innerhalb des World
Domination Seminars im WS17/18 der HfG Karlsruhe entstanden ist. Zwei dieser
Spiele sind [Nosox](#nosox-das-akustikspiel) und
[Highstriker](#highstriker-das-kräftespiel), die jeweils als Webanwendung
implementiert wurden.

Das Repository der beiden Unrealprojekte befindet sich
[hier](https://gitlab.com/jenso/unrealWorldDomination).


## Nosox - Das Akustikspiel

### Anleitung
Nosox ist ein simples zwei Personen Spiel. Dabei haben beide Spieler einen Oberfläche mit neun
unterschiedlichen Knöpfen vor sich. Ein Spieler übernimmt die Rolle des Ansagers, während der Zweite die
Rolle des Zuhörers übernimmt. Der Ansager drückt nun einen der Knöpfe und muss dem Zuhörer nun hinreichend
viele Informationen geben, sodass dieser den gedrückten Knopf ebenfalls drückt. Für jeden korrekt gedrückten
Knopf gibt es einen Punkt dazu, für jeden falschen einen Punkt abgezogen.

### Installation
Klonen des Repositories:
```
git clone https://gitlab.com/jenso/worldDomination.git
```
Anschließend den PeerJS-Servers in [nosox.js:531](./nosox.js#L531), Domain in
[nosox.js:712](./nosox.js#L712) und die Zertifikatpfade in [peer.js:3](./peer.js#L3) anpassen.


Zuletzt noch notwendige Abhängigkeiten installieren:
```
cd worldDomination
bower install
npm install peer
```
Gestartet wird der Peerjs server dann über:
```
node peer.js
```

### Kompatibilitätsprobleme
Aus unerklärlichen Gründen funktioniert als Server (Audioausgabe) nur Firefox; als Client (Audioeingabe) nur Chrome :(


### Danksagung
[urtzurd html-audio gitlab repository](https://github.com/urtzurd/html-audio) aus dem die PitchShifter Logik
stammt.



## Highstriker - Das Kräftespiel

### Anleitung
Highstriker oder auch Hau den Lukas ist ein Spiel, bei dem der Spieler eine Messung seiner Kraft visuell dargestellt bekommt.
Der Spieler öffnet die Seite auf seinem mobilen Endgerät (vorzugsweise in Firefox), gibt seinen Namen an und stellt anschließend
die Kräftemodifikation zwischen Maus und Bär ein. Nun muss der Spieler sein mobiles Endgerät schwingen und bekommt nach der
Kraftmessung diese angezeigt.

### Installation
Falls noch nicht geschehen, sollte das Repository wie oben geklont werden. Anschließend die Index Seite aufrufen. 
Über das Burgemenu gelangt man auf die [striker.html](./striker.html) seite
### Kompatibilitätsprobleme
Sollte unter Firefox funktionieren. Chrome hat etwas gezickt