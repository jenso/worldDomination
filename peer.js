var fs = require("fs");
var PeerServer = require("peer").PeerServer;
var privatekeyPath = "/YOUR/PRIVATEKEY/PATH.PEM"
var certPath = "/YOUR/CERTIFICATE/PATH.PEM"

var server = PeerServer({
  port: 9000,
  debug: true,
  path: "/",
  ssl: {
    key: fs.readFileSync(privatekeyPath),
    cert: fs.readFileSync(certPath)
  }
});
