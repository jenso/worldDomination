if (navigator.serviceWorker) {
      navigator.serviceWorker.register('./service-worker.js', {scope: './about'})
          .then(function (registration) {
                        console.log(registration);
                    })
          .catch(function (e) {
                        console.error(e);
                    })
} else {
      console.log('Service Worker is not supported in this browser.');
}

var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
    '/',
    '/bower_components/jquery/dist/jquery.min.js',
    '/bower_components/material-design-lite/material.min.css',
    '/bower_components/material-design-lite/material.min.js',
    '/bear.png',
    '/hamster.png',
    '/highstriker.js',
    '/striker.html',
    '/strikerStyle.css',
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
    .then(function(cache) {
      console.log('Opened cache');
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
    .then(function(response) {
      // Cache hit - return response
      if (response) {
        return response;
      }
      return fetch(event.request);
    }
    )
  );
});
