class Drawable{
  constructor(){
    this.color = null;
    this.strokeStyle = null;
    this.context = null;
  }

  withContext(context){
    this.context = context;
    return this;
  }

  withColor(color){
    this.color = color;
    return this;
  }

  withStrokeStyle(strokeStyle){
    this.strokeStyle = strokeStyle;
    return this;
  }

  draw(){}
}

class Circle extends Drawable{
  constructor(){
    super();
  }
  draw(){
    this.context.beginPath(),
    this.context.arc(50, 50, 25, 0, 2*Math.PI, false);
    this.context.fillStyle = this.color;
    this.context.fill();
    this.context.lineWidth = 5;
    this.context.strokeStyle= this.strokeStyle;
    this.context.stroke();
    return this;
  }
}

class Rectangle extends Drawable{
  constructor(){
    super();
  }
  draw(){
    this.context.beginPath();
    this.context.rect(25, 25, 50, 50);
    this.context.fillStyle = this.color;
    this.context.fill();
    this.context.lineWidth = 5;
    this.context.strokeStyle = this.strokeStyle;
    this.context.stroke();
    return this;
  }
}

class Triangle extends Drawable{
  constructor(){
    super();
  }
  draw(){
    this.context.beginPath(),
    this.context.moveTo(25,25);
    this.context.lineTo(75,25);
    this.context.lineTo(50,75);
    this.context.closePath();
    this.context.fillStyle = this.color;
    this.context.fill();
    this.context.lineWidth = 5;
    this.context.strokeStyle= this.strokeStyle;
    this.context.stroke();
    return this;
  }
}

class Text extends Drawable{
  constructor(){
    super();
    this.text = "";
  }

  withText(text){
    this.text = text;
    return this;
  }

  draw(){
    this.context.font = "45px Arial";
    this.context.fillText(this.text, 35, 55);
    return this;
  }
}

class Button {
  constructor(){
    this.htmlString = '<div style="display: table-cell" class="mdl-card mdl-shadow--2dp mdl-button"></div>';
    this.symbol = null;
    this.color = null;
    this.text = null;
    this.strokeStyle = null;
    this.drawable = null;
    this.jsonObject = null;
    this.triggerList = {};
  }

  withSymbol(symbol){
    this.symbol = symbol;
    if(symbol == "triangle"){
      this.drawable = new Triangle();
    }else if(symbol == "circle"){
      this.drawable = new Circle();
    }else if(symbol == "rectangle"){
      this.drawable = new Rectangle();
    }else if(symbol == "text"){
      this.drawable = new Text();
    }
    return this;
  }

  withColor(color){
    this.color = color;
    if(color == "red"){
      this.strokeStyle = "#330000";
    }else if(color == "green"){
      this.strokeStyle = "#003300";
    }else if(color == "blue"){
      this.strokeStyle = "#000033";
    }else{
      this.strokeStyle = "#000";
    }
    return this;
  }
  withText(text){
    if(this.drawable instanceof Text){
      this.drawable.text = text;
    }
      this.text = text;
    return this;
  }

  withOn(trigger, func){
    this.triggerList[trigger] = func;
    return this;
  }

  addTriggerlistToObject(){
    if(this.jsonObject){
      for(var key in this.triggerList){
        if(this.triggerList.hasOwnProperty(key)){
          this.jsonObject.on(key, this.triggerList[key]);
        }
      }
    }
    return this;
  }

  render(){
    var canvas = $('<canvas width="100px" height="100px"></canvas>'); 
    canvas.css("width", "75px");
    canvas.css("height", "75px");
    var ctx = canvas[0].getContext("2d");

    this.drawable
      .withContext(ctx)
      .withColor(this.color)
      .withStrokeStyle(this.strokeStyle)
      .draw();
    this.jsonObject = $(this.htmlString).append(canvas);
    this.addTriggerlistToObject();
    return this.jsonObject;
  }

  on(trigger, func){
    if(this.jsonObject){
      this.jsonObject.on(trigger, func);
    }
    this.withOn(trigger, func);
  }

  setBackgroundColor(color){
    if(color == "correct"){
      $(this.drawable.context.canvas).css("background-color", "#0f0");
    }else if(color == "wrong"){
      $(this.drawable.context.canvas).css("background-color", "#f00");
    }
  }

  resetBackgroundColor(){
    $(this.drawable.context.canvas).css("background-color", "rgba(255, 255, 255, 0)");
  }
}


class Message{
  constructor(key, role){
    this.key = key;
    this.role = role;
  }
}

class EventCorElement{

  constructor(key){
    this.key = key;
    this.next = null;
  }

  check(message){
    if(message.key == this.key){
      this.handle(message);
    }else{
      if(this.next != null){
        this.next.check(message);
      }else{
        console.error("no handler for message of type: " + message.key, message);
      }
    }
  }

  handle(message){}

}

class ConnectCOR extends EventCorElement{
  constructor(){
    super("connect");
  }

  handle(message){
    nosox.setInfo("connecting to host");
    console.debug("try to establish connection to " + message.id);
    nosox.peerController.call(message.id);
    nosox.peerController.connectedClients.push(nosox.peerController.peer.connect(message.id));

    nosox.peerController.connectedClients[0].on("open", function(){
      nosox.protocolHandler.handle({"key": "connected", "role": nosox.peerController.role})
    });
    nosox.peerController.connectedClients[0].on("data", function(data){
      nosox.protocolHandler.handle(data);
    });
    nosox.peerController.connectedClients[0].on("disconnected", function(){
      nosox.protocolHandler.handle({"key": "disconnected", "role": nosox.peerController.role})
    });
  }
}

class HostCOR extends EventCorElement{
  constructor(){
    super("host");
  }

  handle(message){
    nosox.setInfo("listen...");
    nosox.peerController.peer.on("connection", function(conn){
      nosox.peerController.connectedClients.push(conn);
      nosox.protocolHandler.handle({"key": "connected", "role": nosox.peerController.role})
      conn.on("data", function(data){
        nosox.protocolHandler.handle(data);
      });
    });
    nosox.peerController.peer.on("disconnected", function(){
      nosox.protocolHandler.handle({"key": "disconnected", "role": nosox.peerController.role})
    });

    nosox.peerController.host();
  }
}

class DisconnectCOR extends EventCorElement{
  constructor(){
    super("disconnect");
  }

  handle(message){
    console.debug("try to disconnect");
  }
}
class ConnectedCOR extends EventCorElement{
  constructor(){
    super("connected");
  }

  handle(message){
    if(nosox.peerController.role == "server"){
      nosox.state.waitForPuzzle();
      nosox.setInfo("waiting for secret button");
    }else{
      nosox.setInfo("choose a secret button");
      nosox.state.selectButton();
    }
  }
}

class DisconnectedCOR extends EventCorElement{
  constructor(){
    super("disconnected");
  }

  handle(message){
    nosox.setInfo("disconnected");
    console.debug("disconnected");
  }
}

class SetTargetButtonCOR extends EventCorElement{
  constructor(){
    super("setTargetButton");
  }

  handle(message){
    nosox.setInfo("describe the pressed button");
    if(!nosox.state.isWaitingForCorrectButton()){
      nosox.disableGameButtons();
      this.setSecretButton(message.color, message.symbol, message.text);
      this.sendRequest();
      nosox.state.waitForCorrectButton();
    }
  }

  setSecretButton(color, symbol, text){
    console.debug(color, symbol, text);
    nosox.secretButton.color = color;
    nosox.secretButton.symbol = symbol;
    nosox.secretButton.text = text;
    nosox.getGameButton(color, symbol, text).setBackgroundColor("correct");
  }

  sendRequest(){
    nosox.peerController.send({"key": "requestButtonRequest"});
  }
}

class RequestButtonRequestCOR extends EventCorElement{
  constructor(){
    super("requestButtonRequest");
  }

  handle(message){
    nosox.setInfo("find the correct button");
    console.debug("now send some buttons requests!");
    nosox.state.waitForSolving();
  }
}

class CheckButtonRequestCOR extends EventCorElement{
  constructor(){
    super("checkButtonRequest");
  }

  handle(message){
    console.debug("check button(color: " + message.color + " symbol: " + message.symbol + " text: " + message.text + ") plox");
    if(this.isSecretButton(message.color, message.symbol, message.text)){
      nosox.peerController.send({"key": "correctButtonResponse"});
      nosox.setInfo("correct button was found");

      nosox.resetGameButtonBackgrounds();
      nosox.enableGameButtons();
      nosox.state.selectButton();
      nosox.points++;
    }else{
      nosox.peerController.send({"key": "wrongButtonResponse", "color": message.color, "symbol": message.symbol, "text": message.text});
      nosox.getGameButton(message.color, message.symbol, message.text).setBackgroundColor("wrong");
      nosox.points--;
    }
    nosox.updatePoints();
  }

  isSecretButton(color, symbol, text){
    return nosox.secretButton.color == color && nosox.secretButton.symbol == symbol && nosox.secretButton.text == text;
  }

}

class CorrectButtonResponseCOR extends EventCorElement{
  constructor(){
    super("correctButtonResponse");
  }

  handle(message){
    nosox.setInfo("correct button was found");
    console.debug("correct button was pressed");
    nosox.resetGameButtonBackgrounds();
    nosox.state.waitForPuzzle();
    nosox.points++;
    nosox.updatePoints();
    if(nosox.checkChangeGameMode()){
      this.changeGameMode();
    }
  }

  changeGameMode(){
    nosox.protocolHandler.handle({"key": "changeGameMode", "gameMode": "hard"});
    nosox.peerController.send({"key": "changeGameMode", "gameMode": "hard"});
  }
}

class FalseButtonResponseCOR extends EventCorElement{
  constructor(){
    super("wrongButtonResponse");
  }

  handle(message){
    nosox.setInfo("nope, wrong button");
    console.debug("wrong button was pressed");
    nosox.getGameButton(message.color, message.symbol, message.text).setBackgroundColor("wrong");
    nosox.points--;
    nosox.updatePoints();
  }
}


class GameModeCOR extends EventCorElement{
  constructor(){
    super("changeGameMode");
  }

  handle(message){
    nosox.setInfo("changed gamemode to: " + message.gameMode);
    nosox.setGameMode(message.gameMode);
  }
}

class ProtocolHandler extends EventCorElement{
  constructor(){
    super("");
    this.lastHandler = this;
    this
      .add(new HostCOR())
      .add(new ConnectCOR())
      .add(new DisconnectCOR())
      .add(new ConnectedCOR())
      .add(new DisconnectedCOR())
      .add(new SetTargetButtonCOR())
      .add(new CheckButtonRequestCOR())
      .add(new CorrectButtonResponseCOR())
      .add(new RequestButtonRequestCOR())
      .add(new GameModeCOR())
      .add(new FalseButtonResponseCOR());
  }
  
  add(handler){
    this.lastHandler.next = handler;
    this.lastHandler = handler;
    return this;
  }
  handle(message){
    if(this.next != null){
      this.next.check(message);
    }
  }
}
class PeerController{
  constructor(){
    this.peer = null;
    this.role = null;
    this.audioCtx = null;
    this.pitchShifter = null;
    this.connectedClients = [];
    this.setupAudio();
    this.setupPeer();
  }

  getUserMedia(){
    //return navigator.mediaDevices.getUserMedia;
    return (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
  }

  call(id){
    console.debug("call");
    this.role = "client";
    var self = this;
    var userMedia = this.getUserMedia();
    //navigator.webkitGetUserMedia({video: true, audio: true}, function(stream) {
    userMedia({video: false, audio: true}, function(stream) {
      var call = self.peer.call(id, stream);
      call.on('stream', function(remoteStream) {
        console.debug(remoteStream);
      });
    }, function(err) {
      console.log('Failed to get local stream' ,err);
    });
  }

  host(){
    this.role = "server";
    var userMedia = this.getUserMedia();
    var self = this;
    this.peer.on('call', function(call) {
      console.debug("caaaall");
      userMedia({video: false, audio: true}, function(stream) {
        call.answer(stream); // Answer the call with an A/V stream.
        call.on('stream', function(remoteStream) {
          self.playStream(remoteStream);
        });
      }, function(err) {
        console.log('Failed to get local stream' ,err);
      });
    });
    console.debug("listen...");
  }

  send(data){
    for(var i in this.connectedClients){
      if(this.connectedClients[i].open){
        this.connectedClients[i].send(data);
      }
    }
  }

  playStream(stream) {
    var audioCtx = this.audioCtx;
    var source = audioCtx.createMediaStreamSource(stream);
    this.pitchShifter = new PitchShifterProcessor(audioCtx);

    //source.connect(audioCtx.destination);
    source.connect(this.pitchShifter.getProcessor());
    this.pitchShifter.connect(audioCtx.destination);
    console.debug(this.pitchShifter);

    //var gainNode = audioCtx.createGain();
    //gainNode.connect(audioCtx.destination);
  }

  setupAudio(){
    this.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  }

  setupPeer(){
    self = this;
    this.peer = new Peer({key: 'peerjs', secure: true, host: "zeeeg.nsupdate.info", port:9000, path: "/"});
    this.peer.on("open", function(){
      nosox.setupQRCode();
    });
    this.peer.on("error", console.debug);
  }
}

class StateMachine{
  constructor(){
    this.state = "init";
  }

  waitForPuzzle(){
    this.state = "waitForPuzzle";
  }
  isWaitingForPuzzle(){
    return this.state == "waitForPuzzle";
  }

  hasPuzzle(){
    return this.state == "waitForSolving";
  }

  selectButton(){
    this.state = "selectButton";
  }

  hasToSelectButton(){
    return this.state == "selectButton";
  }

  waitForCorrectButton(){
    this.state = "waitForCorrectButton";
  }

  isWaitingForCorrectButton(){
    return this.state == "waitForCorrectButton";
  }

  waitForSolving(){
    this.state = "waitForSolving";
  }

}

class Nosox {
  constructor(){
    this.peerController = new PeerController();
    this.protocolHandler = new ProtocolHandler();
    this.state = new StateMachine();
    this.secretButton = {"color" : null, "symbol": null, "text": null};
    this.gameButtons = [];
    this.setup();
    this.points = 0;
    this.gamemode = null;
  }

  setup(){
    this.setupConnectButton();
    this.setupHostButton();
    this.setGameMode("easy");
  }

  setupConnectButton(){
    $("#connect").on("click", function(){
      var id = window.prompt("hostname plox");
      nosox.protocolHandler.handle({"key": "connect", "id": id});
    });
  }
  
  setupHostButton(){
    $("#host").on("click", function(){
      nosox.protocolHandler.handle({"key": "host"});
    });
  }

  addEasyButtons(){
    for(var color of ["red", "green", "blue"]){
      for(var icon of ["triangle", "rectangle", "circle"]){
        var button = this.addButton(icon, color);
        this.gameButtons.push(button);
      }
    }
  }

  addHardButtons(){
    var colors = ["red", "green", "blue"];
    var text = ["ø", "æ", "ŧ", "@", " ", "þ", "«", "ħ","ಠ"];
    for(var idx in text){
      var button = this.addButton("text", colors[idx%3], text[idx]);
      this.gameButtons.push(button);
    }
  }

  addButton(symbol, color, text){
    var button = new Button()
      .withColor(color)
      .withSymbol(symbol)
      .withText(text);
    $("." + color +"cell").append(button.render());
    button.on("click", function(){nosox.gameButtonClicked(button)});
    return button;
  }

  gameButtonClicked(button){
    if(this.state.hasPuzzle()){
      nosox.peerController.send({"key": "checkButtonRequest", "color": button.color, "symbol" : button.symbol, "text": button.text });
    }else if(this.state.hasToSelectButton()){
      nosox.protocolHandler.handle({"key": "setTargetButton", "color": button.color, "symbol" : button.symbol, "text": button.text });
    }else{
      nosox.setInfo("nope, not yet!")
    }
  }

  disableGameButtons(){
    for(var i in this.gameButtons){
      this.gameButtons[i].jsonObject[0].setAttribute("disabled", "disabled");
    }
  }

  enableGameButtons(){
    for(var i in this.gameButtons){
      this.gameButtons[i].jsonObject[0].removeAttribute("disabled");
    }
  }

  getGameButton(color, symbol, text){
    for(var i in this.gameButtons){
      if(this.gameButtons[i].color == color && this.gameButtons[i].symbol == symbol && this.gameButtons[i].text == text){
        return this.gameButtons[i];
      }
    }
  }

  resetGameButtonBackgrounds(){
    for(var i in this.gameButtons){
      this.gameButtons[i].resetBackgroundColor();
    }
  }

  clearGameButtons(){
    for(var i in this.gameButtons){
      this.gameButtons[i].jsonObject.remove();
    }
    this.gameButtons = [];
  }

  setGameMode(mode){
    if(mode == "easy"){
      this.setEasyGameMode();
      this.gamemode = mode;
    }else if(mode == "hard"){
      this.setHardGameMode();
      this.gamemode = mode;
    }
  }

  setEasyGameMode(){
    this.clearGameButtons();
    this.addEasyButtons();
  }

  setHardGameMode(){
    this.clearGameButtons();
    this.addHardButtons();
  }

  checkChangeGameMode(){
    return this.points >= 5 && this.gamemode != "hard";
  }

  setInfo(text){
    $("#headline").text(text);
  }
  updatePoints(){
    $("#points").text("Points: " + this.points);
  }

  setupQRCode(){
    $("#peerId").text(this.peerController.peer.id);
    new QRCode($("#qr")[0], "https://hfg.yenso.de/index.html?peerId=" + this.peerController.peer.id);
  }


}


class Visualizer{
  constructor(analyser){
    this.analyser = analyser;
    this.canvas = $("#visualizer")[0];
    this.canvasCtx = this.canvas.getContext("2d");
    this.canvas.setAttribute("width", "200px");
    this.canvas.setAttribute("height", "56px");
    this.visualize();
  }
  visualize(){
    this.WIDTH = this.canvas.width;
    this.HEIGHT = this.canvas.height;
    this.analyser.fftSize = 2048;
    var bufferLength = this.analyser.fftSize;
    var dataArray = new Uint8Array(bufferLength);
    this.canvasCtx.clearRect(0,0,this.WIDTH,this.HEIGHT);
    self = this;

    var draw = function() {
      requestAnimationFrame(draw);
      self.analyser.getByteTimeDomainData(dataArray);
      self.canvasCtx.fillStyle = 'rgb(200, 200, 200)';
      self.canvasCtx.fillRect(0, 0, self.WIDTH, self.HEIGHT);
      self.canvasCtx.lineWidth = 2;
      self.canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
      self.canvasCtx.beginPath();
      var sliceWidth = self.WIDTH * 1.0 / bufferLength;
      var x = 0;
      for(var i = 0; i < bufferLength; i++) {
        var v = dataArray[i] / 128.0;
        var y = v * self.HEIGHT/2;
        if(i === 0) {
          self.canvasCtx.moveTo(x, y);
        } else {
          self.canvasCtx.lineTo(x, y);
        }
        x += sliceWidth;
      }
      self.canvasCtx.lineTo(self.canvas.width, self.canvas.height/2);
      self.canvasCtx.stroke();
    };
    draw();
  }
}

function GetURLParameter(sParam)
{
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');
  for (var i = 0; i < sURLVariables.length; i++)
  {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam)
    {
      return sParameterName[1];
    }
  }
}

$().ready(function(){
  window.nosox = new Nosox();
  window.urlID = GetURLParameter("peerId")
  if (urlID){
    nosox.protocolHandler.handle({"key": "connect", "id": urlID});
  }

});
