<?php
  
  function cmp($a, $b){
    return $b[1] - $a[1];
  }

  function writeHighscore($highscore){
    file_put_contents("highscore.json", json_encode($highscore));
  }

  function addHighscore($name, $points){
    $highscore = loadHighscore();
    array_push($highscore, array($name, $points, time()));
    usort($highscore, "cmp");
    if(sizeof($highscore) > 3){
      array_pop($highscore);
    }
    writeHighscore($highscore);
  }

  function resetHighscore(){
    writeHighscore(array());
  }

  function loadHighscore(){
    $highscore = file_get_contents("highscore.json");
    if($highscore){
      return json_decode($highscore);
    }else{
      $obj = array();
      return $obj;
    }
  }

  header('Content-Type: application/json');
  if(isset($_GET["get"])){
    echo(json_encode(loadHighscore()));
  }elseif(isset($_GET["add"])){
    addHighscore($_GET["name"], $_GET["points"]);
    echo(json_encode(loadHighscore()));
  }elseif(isset($_GET["reset"])){
    resetHighscore();
    echo("[]");
  }
?>
