class PitchShifterProcessor {

  constructor(audioContext){
    this.audioContext = audioContext;
    this.pitchShifterProcessor = null;
    this.validGranSizes = [256, 512, 1024, 2048, 4096, 8192];
    this.grainSize = this.validGranSizes[1];
    this.pitchRatio = 4.0;
    this.overlapRatio = 0.50;

  }

  withPitchRatio(pitchRatio){
    this.pitchRatio = pitchRatio;
    return this;
  }

  withOverlapRatio(overlapRatio){
    this.overlapRatio = overlapRatio;
    return this;
  }

  withGrainSize(grainSize){
    if(grainSize%256 == 0){
      this.grainSize = grainSize;
    }
    return this;
  }

  init(){
    if(!this.audioContext){
      console.error("audioContext is not set");
      return this;
    }
    if (this.pitchShifterProcessor) {
        this.pitchShifterProcessor.disconnect();
    }

    if (this.audioContext.createScriptProcessor) {
        this.pitchShifterProcessor = this.audioContext.createScriptProcessor(this.grainSize, 1, 1);
    } else if (this.audioContext.createJavaScriptNode) {
        this.pitchShifterProcessor = this.audioContext.createJavaScriptNode(this.grainSize, 1, 1);
    }

    self = this;
    this.pitchShifterProcessor.buffer = new Float32Array(this.grainSize * 2);
    this.pitchShifterProcessor.grainWindow = this.hannWindow(this.grainSize);
    this.pitchShifterProcessor.onaudioprocess = function (event) {

        var inputData = event.inputBuffer.getChannelData(0);
        var outputData = event.outputBuffer.getChannelData(0);

        for (i = 0; i < inputData.length; i++) {

            // Apply the window to the input buffer
            inputData[i] *= this.grainWindow[i];

            // Shift half of the buffer
            this.buffer[i] = this.buffer[i + self.grainSize];

            // Empty the buffer tail
            this.buffer[i + self.grainSize] = 0.0;
        }

        // Calculate the pitch shifted grain re-sampling and looping the input
        var grainData = new Float32Array(self.grainSize * 2);
        for (var i = 0, j = 0.0;
             i < self.grainSize;
             i++, j += self.pitchRatio) {

            var index = Math.floor(j) % self.grainSize;
            var a = inputData[index];
            var b = inputData[(index + 1) % self.grainSize];
            grainData[i] += self.linearInterpolation(a, b, j % 1.0) * this.grainWindow[i];
        }

        // Copy the grain multiple times overlapping it
        for (i = 0; i < self.grainSize; i += Math.round(self.grainSize * (1 - self.overlapRatio))) {
            for (j = 0; j <= self.grainSize; j++) {
                this.buffer[i + j] += grainData[j];
            }
        }

        // Output the first half of the buffer
        for (i = 0; i < self.grainSize; i++) {
            outputData[i] = this.buffer[i];
        }
    };
    return this;
  }

  hannWindow(length){
    var window = new Float32Array(length);
    for (var i = 0; i < length; i++) {
      window[i] = 0.5 * (1 - Math.cos(2 * Math.PI * i / (length - 1)));
    }
    return window;
  }

  linearInterpolation(a, b, t){
    return a + (b - a) * t;
  }

  connect(other){
    if(!this.pitchShifterProcessor){
      this.init();
    }
    this.pitchShifterProcessor.connect(other);
  }

  disconnect(other){
    if(this.pitchShifterProcessor){
      this.pitchShifterProcessor.disconnect(other);
    }
  }
  getProcessor(){
    if(!this.pitchShifterProcessor){
      this.init();
    }
    return this.pitchShifterProcessor;
  }

}
